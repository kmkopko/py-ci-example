#
# some tests for fcns.py
#

import unittest

from project.fcns import ( times_two, sum_iterable, )

class Test_times_two( unittest.TestCase ):
    
    def test_integer( self ):
        for i in xrange( -100, 100 ):
            self.assertEqual( i * 2, times_two( i ) )

    def test_string( self ):
        self.assertEqual( 'aa', times_two( 'a' ) )
        self.assertEqual( 'abcabc', times_two( 'abc' ) )
        
    def test_float( self ):
        self.assertEqual( -1.2, times_two( -0.6 ) )
        self.assertEqual( 0.0, times_two( 0.0 ) )
        self.assertEqual( 4.6, times_two( 2.3 ) )
        
class Test_sum_iterable( unittest.TestCase ):

    def test_list( self ):
        self.assertEqual( 6, sum_iterable( [ 1, 2, 3 ] ) )

    def test_noniterable( self ):
        self.assertEqual( 0, sum_iterable( None ) )


