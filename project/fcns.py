#
# a few simple functions that should be tested
#

def times_two( x ):
    return x * 2

def sum_iterable( iterable ):
    running_sum = 0
    try:
        it = iter( iterable )
        for i in it:
            running_sum += i
    except TypeError:
        pass
    return running_sum


